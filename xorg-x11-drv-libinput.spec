%global moduledir %(pkg-config xorg-server --variable=moduledir )
%global driverdir %{moduledir}/input

Name:          xorg-x11-drv-libinput
Version:       1.5.0
Release:       1
Summary:       Xorg X11 libinput input driver
License:       MIT
URL:           https://www.x.org
Source0:       https://www.x.org/releases/individual/driver/xf86-input-libinput-%{version}.tar.xz
Source1:       71-libinput-overrides-wacom.conf

Patch0001:     0001-Add-a-DPIScaleFactor-option-as-temporary-solution-to.patch

BuildRequires: gcc make
BuildRequires: pkgconfig(inputproto) >= 2.2
BuildRequires: pkgconfig(libinput) >= 1.11.0
BuildRequires: pkgconfig(xorg-macros) >= 1.8
BuildRequires: pkgconfig(xorg-server) >= 1.19
BuildRequires: pkgconfig(xproto)
BuildRequires: libudev-devel

Requires:      Xorg %(xserver-sdk-abi-requires ansic) xkeyboard-config libinput >= 0.21.0
Requires:      Xorg %(xserver-sdk-abi-requires xinput)

Provides:      xorg-x11-drv-synaptics = 1.9.0-3
Obsoletes:     xorg-x11-drv-synaptics < 1.9.0-3

%description
A generic input driver for the X.Org X11 X server based on libinput,
supporting all devices.

%package        devel
Summary:        Xorg X11 libinput input driver development package.

%description    devel
Xorg X11 libinput input driver development files.

%package_help

%prep
%autosetup -n xf86-input-libinput-%{version} -p1

%build
%configure --disable-static --disable-silent-rules
%make_build

%check
%make_build check

%install
%make_install
%delete_la

cp %{S:1} $RPM_BUILD_ROOT%{_datadir}/X11/xorg.conf.d/

%files
%license COPYING
%doc ChangeLog
%{driverdir}/libinput_drv.so
%{_datadir}/X11/xorg.conf.d/*.conf

%files devel
%{_libdir}/pkgconfig/xorg-libinput.pc
%{_includedir}/xorg/libinput-properties.h

%files help
%doc README.md
%{_mandir}/man4/libinput.4*

%changelog
* Tue Oct 15 2024 Funda Wang <fundawang@yeah.net> - 1.5.0-1
- update to version 1.5.0

* Thu Dec 28 2023 Paul Thomas <paulthomas100199@gmail.com> - 1.4.0-1
- update to version 1.4.0

* Fri Jul 14 2023 zhangpan <zhangpan103@h-partners.com> - 1.3.0-1
- update to 1.3.0

* Fri Feb 03 2023 zhangpan <zhangpan@h-partners.com> - 1.2.1-1
- update to 1.2.1

* Tue Oct 25 2022 wangkerong <wangkerong@h-partners.com> - 1.2.0-2
- rebuild for next release

* Thu Nov 11 2021 yangcheng<yangcheng87@huawei.com> - 1.2.0-1
- Upgrade to 1.2.0-1

* Mon Jul 20 2020 chengguipeng<chengguipeng1@huawei.com> - 0.30.0-1
- Upgrade to 0.30.0-1

* Wed Feb 12 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.28.0-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:enable make check test case

* Thu Jan 3 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.28.0-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:optimization the spec

* Fri Oct 11 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.28.0-3
- Package init 
